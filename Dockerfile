# Utilisez une image OpenJDK pour exécuter l'application
FROM openjdk:17

ARG JAR_FILE=target/*.jar

COPY ${JAR_FILE} app.jar

# Exposez le port sur lequel l'application s'exécute
EXPOSE 8080

# Commande pour exécuter l'application lorsqu'un conteneur est lancé
CMD ["java", "-jar"]
